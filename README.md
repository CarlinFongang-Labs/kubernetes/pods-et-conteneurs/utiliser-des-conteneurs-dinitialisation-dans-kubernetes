# Utilisation d'un Conteneur d'Initialisation pour Retarder le Démarrage d'un Pod


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

1. Créer un exemple de pod qui utilise un conteneur d'initialisation pour retarder le démarrage


2. Testez votre configuration en créant le service et en vérifiant le démarrage du pod

# Contexte

Vous travaillez pour BeeBox, une entreprise qui assure des expéditions régulières d'abeilles aux clients. L'entreprise est en train de déployer une application de statut d'expédition sur le cluster.

Les développeurs créent un composant d'application conçu pour s'exécuter dans un pod Kubernetes. Ce composant dépend d'un service Kubernetes appelé `shipping-svc`, et ils souhaiteraient que leur conteneur d'application retarde le démarrage lorsque ce service n'est pas disponible dans le cluster. Une fois le service disponible, le conteneur d'application principal doit procéder au démarrage.

Votre tâche consiste à créer une preuve de concept montrant comment concevoir un pod qui retardera le démarrage des conteneurs d'applications jusqu'à ce que le service soit disponible.

Remarque : Veuillez utiliser BusyBox 1.27. Les versions plus récentes peuvent rencontrer des complications avec `nslookup`.

>![Alt text](img/image.png)

# Application

#### Étape 1 : Connexion au Serveur de Laboratoire

Connectez-vous au serveur de laboratoire fourni à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@PUBLIC_IP_ADDRESS_CONTROL_NODE
```


#### Étape 2 : Création d'un Pod avec un Conteneur d'Initialisation

Ouvrez le fichier descripteur de pod :

```sh
nano pod.yml
```

Ajoutez un conteneur init (au même niveau que `containers` dans le fichier) pour retarder le démarrage jusqu'à ce que le service `shipping-svc` soit disponible :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: shipping-web
spec:
  containers:
  - name: nginx
    image: nginx:1.19.1
  initContainers:
  - name: shipping-svc-check
    image: busybox:1.27
    command: ['sh', '-c', 'until nslookup shipping-svc; do echo waiting for shipping-svc; sleep 2; done']
  containers:
  - name: main-container
    image: nginx
```

Enregistrez et quittez le fichier.

#### Étape 3 : Création du Pod

Créez le pod en utilisant la commande suivante :

```sh
kubectl create -f pod.yml
```

>![Alt text](img/image-1.png)
*Pod crée*

#### Étape 4 : Vérification de l'État du Pod

Vérifiez l'état du pod avec la commande suivante :

```sh
kubectl get pods
```
>![Alt text](img/image-2.png)

Le pod devrait rester dans le statut `Init` jusqu'a ce que le serivce shipping-svc soit disponible

#### Étape 5 : Test de la Configuration


```bash
nano shipping-svc.yml
```
contenu du manifest `shipping-svc.yml`

```yaml
apiVersion: v1
kind: Service
metadata:
  name: shipping-svc
spec:
  selector:
    app: shipping-svc
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
---
apiVersion: v1
kind: Pod
metadata:
  name: shipping-backend
  labels:
   app: shipping-svc
spec:
  containers:
  - name: nginx
    image: nginx:1.19.1
```

Créez le service à partir du fichier `shipping-svc.yml` :

```sh
kubectl create -f shipping-svc.yml
```

>![Alt text](img/image-3.png)
*Le service à bien été crée*

#### Étape 6 : Vérification de l'État du Pod après Création du Service

Vérifiez à nouveau l'état de votre pod :

```sh
kubectl get pods
```

>![Alt text](img/image-4.png)
*Pods disponible*


Le pod devrait entrer dans le statut `Running` après environ une minute.

En sommes, nous avons réussi à conditionné le démarrage du pod `shipping-web` par celui d'un autre pod (`shipping-backend`) que nous avons exécuté plus tard